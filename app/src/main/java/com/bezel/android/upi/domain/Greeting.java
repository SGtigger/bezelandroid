package com.bezel.android.upi.domain;

import lombok.Data;

/**
 * Created by ahamed.nijamudeen on 3/10/2016.
 */
@Data
public class Greeting {

    private String id;
    private String content;
}
