package com.bezel.android.upi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by ahamed.nijamudeen on 3/10/2016.
 */
public class Payee implements Serializable{
    @JsonProperty("payee")
    String virtualAddr;

    public String getVirtualAddr() {
        return virtualAddr;
    }

    public void setVirtualAddr(String virtualAddr) {
        this.virtualAddr = virtualAddr;
    }
}
