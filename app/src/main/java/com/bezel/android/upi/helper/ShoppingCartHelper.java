package com.bezel.android.upi.helper;

import android.content.res.Resources;
import com.bezel.android.upi.R;
import com.bezel.android.upi.domain.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahamed.nijamudeen on 3/3/2016.
 */
public class ShoppingCartHelper {

    public static final String PRODUCT_INDEX = "PRODUCT_INDEX";

    private static List<Product> catalog;
    private static List<Product> cart;

    public static List<Product> getCatalog(Resources res) {
        if (catalog == null) {
            catalog = new ArrayList<Product>();
            catalog.add(new Product("Carrot", res
                    .getDrawable(R.drawable.ic_carrot,null),
                    "Fresh Ooty Carrot","1kg", 29.99));
            catalog.add(new Product("Lettuce", res
                    .getDrawable(R.drawable.ic_lettuce,null),
                    "Fresh Singapore Lettuce","1kg", 24.99));
            catalog.add(new Product("Tomato", res
                    .getDrawable(R.drawable.ic_tomato,null),
                    "Fresh New Zealand Tomato","1kg", 14.99));
            catalog.add(new Product("Cucumber", res
                    .getDrawable(R.drawable.ic_cucumber,null),
                    "Fresh Bangalore Cucumber","1kg", 14.99));
        }

        return catalog;
    }

    public static List<Product> getCart() {
        if (cart == null) {
            cart = new ArrayList<Product>();
        }

        return cart;
    }

    public static void clearCart(){
        getCart().clear();
    }
}
