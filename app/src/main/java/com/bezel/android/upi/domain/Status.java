package com.bezel.android.upi.domain;

/**
 * Created by ahamed.nijamudeen on 3/15/2016.
 */
public enum Status {
    SUCCESS,PENDING,FAILED,PROCESSING;
}
