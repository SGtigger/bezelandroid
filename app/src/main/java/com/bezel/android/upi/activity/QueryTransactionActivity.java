package com.bezel.android.upi.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.bezel.android.upi.R;
import com.bezel.android.upi.domain.Result;
import com.bezel.android.upi.domain.Transaction;
import com.bezel.android.upi.helper.ListTransactionHelper;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahamed.nijamudeen on 3/17/2016.
 */
public class QueryTransactionActivity extends Activity {

    private static final String TAG = QueryTransactionActivity.class.getSimpleName();
    private TextView transactionId,statusView,date,amount;
    private Button done;
    private int transactionIndex;
    Transaction transaction;
    DecimalFormat df = new DecimalFormat("0.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_query_transaction);
//            getLayoutInflater().inflate(R.layout.activity_transaction,frameLayout);
//            mDrawerList.setItemChecked(position,true);

            transactionIndex = getIntent().getExtras().getInt(ListTransactionHelper.PRODUCT_INDEX);
//            transactionIndex = 0;

            new HttpRequestTask().execute();
//            LayoutInflater inflater = (LayoutInflater) this
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View contentView = inflater.inflate(R.layout.activity_transaction, null, false);


//            statusInfo.setText("Payment Submitted Successfully. Your Transaction Id is " + UUID.randomUUID());
        } catch (Exception e) {
            Log.e(TAG,e.getMessage(),e);
        }
    }


    private class HttpRequestTask extends AsyncTask<Void, Void, ResponseEntity> {

        /**
         * Runs on the UI thread before {@link #doInBackground}.
         *
         * @see #onPostExecute
         * @see #doInBackground
         */
        private ProgressDialog progDailog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(QueryTransactionActivity.this);
            progDailog.setMessage("Qyering Transaction Status...");
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(true);
            progDailog.show();
            transactionId = (TextView) findViewById(R.id.QueryTransactionId);
            statusView = (TextView) findViewById(R.id.QueryStatusValue);
            amount = (TextView) findViewById(R.id.QueryAmountValue);
            date = (TextView) findViewById(R.id.QueryDateValue);
            done = (Button) findViewById(R.id.QueryTransactionDoneButton);
            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        @Override
        protected ResponseEntity doInBackground(Void... params) {
            try {
                transaction = ListTransactionHelper.get(transactionIndex, getBaseContext());
                String id = transaction.getTransactionId();
//                final String url = "http://rest-service.guides.spring.io/greeting";
                String url = "https://ec2-52-76-246-125.ap-southeast-1.compute.amazonaws.com/upi/payments/txnId/";
                String finalUrl = url.concat(id);
                RestTemplate restTemplate = new RestTemplate();
                // Create a trust manager that does not validate certificate chains
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return null;
                            }
                            public void checkClientTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                            public void checkServerTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                        }
                };
                HostnameVerifier allHostsValid = new HostnameVerifier()
                {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1)
                    {
                        return true;
                    }
                };
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

//                InetSocketAddress address = new InetSocketAddress("proxy-us.sky.savvis.net", 8080);
//                Proxy proxy = new Proxy(Proxy.Type.HTTP, address);


//                factory.setProxy(proxy);
//                restTemplate.setRequestFactory();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
//                Greeting greeting = restTemplate.getForObject(url, Greeting.class);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                List<MediaType> list = new ArrayList();
                list.add(MediaType.APPLICATION_JSON);
                headers.setAccept(list);
                List<MediaType> accept = new ArrayList();
                accept.add(MediaType.ALL);
                headers.setAccept(accept);
                HttpEntity entity = new HttpEntity(null,headers);
//                HttpEntity entity = new HttpEntity(null,headers);
                ResponseEntity<Result> response = restTemplate.exchange(finalUrl, HttpMethod.GET,entity,Result.class);
//                String response = restTemplate.getForObject(url,String.class);
                return response;
            } catch (Exception e) {
                Log.e("TransactionActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(ResponseEntity response) {
            progDailog.dismiss();
            String status = null;
            transactionId.setText(transaction.getTransactionId());
            amount.setText(df.format(transaction.getAmount()));
            date.setText(transaction.getDate().toString());
            if (response != null && response.getBody()!=null){
                Result result = (Result)response.getBody();
                status = result.getStatus();
            }
            progDailog.dismiss();
            statusView.setText(status);
        }

    }
}
