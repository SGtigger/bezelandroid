package com.bezel.android.upi.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.bezel.android.upi.R;
import com.bezel.android.upi.domain.Transaction;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by ahamed.nijamudeen on 3/15/2016.
 */
public class TransactionAdapter extends BaseAdapter {

    private static final String TAG = TransactionAdapter.class.getSimpleName();

    private List<Transaction> transactions;
    private LayoutInflater inflater;

    public TransactionAdapter(List<Transaction> transactions, LayoutInflater inflater) {
        this.transactions = transactions;
        this.inflater = inflater;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return transactions.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Transaction getItem(int position) {
        return transactions.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return transactions.indexOf(getItem(position));
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ViewHolder viewHolder;
            DecimalFormat df = new DecimalFormat("0.00");
            Transaction transaction = transactions.get(position);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.transaction_items, null);
                convertView.setTag(viewHolder);
                viewHolder.transactionId = (TextView) convertView.findViewById(R.id.TransactionId);
                viewHolder.amount = (TextView) convertView.findViewById(R.id.transactionAmountValue);
                viewHolder.date = (TextView) convertView.findViewById(R.id.DateValue);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.transactionId.setText(transaction.getTransactionId());
            viewHolder.amount.setText(df.format(transaction.getAmount()));
            viewHolder.date.setText(transaction.getDate().toString());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return convertView;
    }

    private class ViewHolder {
        TextView transactionId, amount, date;
    }

}
