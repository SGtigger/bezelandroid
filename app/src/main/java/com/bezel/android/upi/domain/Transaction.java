package com.bezel.android.upi.domain;

import lombok.Data;

import java.util.Date;

/**
 * Created by ahamed.nijamudeen on 3/15/2016.
 */

@Data
public class Transaction implements Comparable<Transaction>{

    private String transactionId;
    private Status status;
    private Date date;
    private double amount;

    public Transaction(String transactionId,Status status, Date date,double amount){
        this.transactionId = transactionId;
        this.status = status;
        this.date = date;
        this.amount = amount;
    }

    @Override
    public int compareTo(Transaction transaction) {
        return transaction.getDate().compareTo(this.getDate());
    }
}
