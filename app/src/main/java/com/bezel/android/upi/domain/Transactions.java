package com.bezel.android.upi.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahamed.nijamudeen on 3/15/2016.
 */
public class Transactions {

    private List<Transaction> transactions;

    public List<Transaction> getTransactions() {
        if (transactions ==null){
            this.transactions = new ArrayList<Transaction>();
        }
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
