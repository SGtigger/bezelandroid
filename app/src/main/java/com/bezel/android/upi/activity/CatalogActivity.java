package com.bezel.android.upi.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import com.bezel.android.upi.R;
import com.bezel.android.upi.adapter.ProductAdapter;
import com.bezel.android.upi.domain.Product;
import com.bezel.android.upi.helper.ShoppingCartHelper;

import java.util.List;

/**
 * Created by ahamed.nijamudeen on 3/3/2016.
 */


public class CatalogActivity extends Activity {

    private List<Product> mProductList;
    protected ListView listViewCatalog;
    protected Button viewShoppingCart,goHome;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_catalog);
//            http://stackoverflow.com/questions/21405958/how-to-display-navigation-drawer-in-all-activities
//            super.replaceContentLayout(R.layout.activity_catalog, super.CONTENT_LAYOUT_ID);

//            LayoutInflater inflater = (LayoutInflater) this
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View contentView = inflater.inflate(R.layout.activity_catalog, null, false);
//            mDrawerLayout.addView(contentView, 0);
//        getLayoutInflater().inflate(R.layout.activity_catalog, frameLayout);
//        new populateUiScreen().execute();
        viewShoppingCart = (Button) findViewById(R.id.ButtonViewCart);
        goHome =(Button) findViewById(R.id.GoHomeCartButton);
//        mDrawerList.setItemChecked(position, true);
        // Create the list
        listViewCatalog = (ListView) findViewById(R.id.CatalogListView);
        listViewCatalog.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent productDetailsIntent = new Intent(getBaseContext(), ProductDetailsActivity.class);
                productDetailsIntent.putExtra(ShoppingCartHelper.PRODUCT_INDEX, position);
                startActivity(productDetailsIntent);
            }
        });


        viewShoppingCart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent viewShoppingCartIntent = new Intent(getBaseContext(), ShoppingCartActivity.class);
                startActivity(viewShoppingCartIntent);
            }
        });

        goHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goHomeIntent = new Intent(getBaseContext(),HomeActivity.class);
                startActivity(goHomeIntent);
            }
        });
        mProductList = ShoppingCartHelper.getCatalog(getResources());
        listViewCatalog.setAdapter(new ProductAdapter(mProductList, getLayoutInflater(), false, getAssets()));

    }

    private class populateUiScreen extends AsyncTask<Void, Void, List<Product>> {

        private ProgressDialog progDailog;

        /**
         * Runs on the UI thread before {@link #doInBackground}.
         *
         * @see #onPostExecute
         * @see #doInBackground
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(CatalogActivity.this);
            progDailog.setMessage("Loading Product...");
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(true);
            progDailog.show();
            viewShoppingCart = (Button) findViewById(R.id.ButtonViewCart);
//            mDrawerList.setItemChecked(position, true);
            // Create the list
            listViewCatalog = (ListView) findViewById(R.id.CatalogListView);
            listViewCatalog.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    Intent productDetailsIntent = new Intent(getBaseContext(), ProductDetailsActivity.class);
                    productDetailsIntent.putExtra(ShoppingCartHelper.PRODUCT_INDEX, position);
                    startActivity(productDetailsIntent);
                }
            });


            viewShoppingCart.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent viewShoppingCartIntent = new Intent(getBaseContext(), ShoppingCartActivity.class);
                    startActivity(viewShoppingCartIntent);
                }
            });

        }

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected List<Product> doInBackground(Void... params) {
            // Obtain a reference to the product catalog
            mProductList = ShoppingCartHelper.getCatalog(getResources());
            return mProductList;
        }

        /**
         * <p>Runs on the UI thread after {@link #doInBackground}. The
         * specified result is the value returned by {@link #doInBackground}.</p>
         * <p>
         * <p>This method won't be invoked if the task was cancelled.</p>
         *
         * @param products The result of the operation computed by {@link #doInBackground}.
         * @see #onPreExecute
         * @see #doInBackground
         * @see #onCancelled(Object)
         */
        @Override
        protected void onPostExecute(List<Product> products) {
            super.onPostExecute(products);
            progDailog.dismiss();
            listViewCatalog.setAdapter(new ProductAdapter(products, getLayoutInflater(), false, getAssets()));
        }
    }
}