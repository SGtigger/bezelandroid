package com.bezel.android.upi.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.bezel.android.upi.R;
import com.bezel.android.upi.domain.OrderPayment;
import com.bezel.android.upi.domain.Result;
import com.bezel.android.upi.domain.Status;
import com.bezel.android.upi.domain.Transaction;
import com.bezel.android.upi.helper.SharedPreferenceHelper;
import com.bezel.android.upi.helper.ShoppingCartHelper;
import com.google.android.gms.wallet.NotifyTransactionStatusRequest;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TransactionActivity extends Activity {

    private static final String TAG = TransactionActivity.class.getSimpleName();
    private TextView statusInfo;
    private DecimalFormat df = new DecimalFormat("0.00");

    Button goHome,checkTransactions;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_transaction);
//            getLayoutInflater().inflate(R.layout.activity_transaction,frameLayout);
//            mDrawerList.setItemChecked(position,true);

            new HttpRequestTask().execute();
//            LayoutInflater inflater = (LayoutInflater) this
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View contentView = inflater.inflate(R.layout.activity_transaction, null, false);

//            statusInfo.setText("Payment Submitted Successfully. Your Transaction Id is " + UUID.randomUUID());
        } catch (Exception e) {
            Log.e(TAG,"Error Performing Transaction..",e);
        }
    }


    private class HttpRequestTask extends AsyncTask<Void, Void, ResponseEntity<Result>> {

        /**
         * Runs on the UI thread before {@link #doInBackground}.
         *
         * @see #onPostExecute
         * @see #doInBackground
         */
        private ProgressDialog progDailog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            statusInfo = (TextView) findViewById(R.id.transactionStatus);
            progDailog = new ProgressDialog(TransactionActivity.this);
            progDailog.setMessage("Transaction In Progress...");
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(true);
            progDailog.show();
            goHome = (Button) findViewById(R.id.GoHomeTransactionButton);
            checkTransactions = (Button) findViewById(R.id.checkTransactionsButton);
            goHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(getBaseContext(),HomeActivity.class);
                    startActivity(intent);
                }
            });

            checkTransactions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(getBaseContext(),ListTransactionActivity.class);
                    startActivity(intent);
                }
            });
        }

        @Override
        protected ResponseEntity<Result> doInBackground(Void... params) {
            ResponseEntity<Result> response = null;
            try {
                OrderPayment payment = (OrderPayment) getIntent().getExtras().getSerializable("PaymentOrder");

//                final String url = "http://rest-service.guides.spring.io/greeting";
                final String url = "https://ec2-52-76-246-125.ap-southeast-1.compute.amazonaws.com/upi/payments";
                RestTemplate restTemplate = new RestTemplate();
                // Create a trust manager that does not validate certificate chains
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return null;
                            }
                            public void checkClientTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                            public void checkServerTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                        }
                };
                HostnameVerifier allHostsValid = new HostnameVerifier()
                {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1)
                    {
                        return true;
                    }
                };
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

//                InetSocketAddress address = new InetSocketAddress("proxy-us.sky.savvis.net", 8080);
//                Proxy proxy = new Proxy(Proxy.Type.HTTP, address);


//                factory.setProxy(proxy);
//                restTemplate.setRequestFactory();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
//                Greeting greeting = restTemplate.getForObject(url, Greeting.class);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                List<MediaType> list = new ArrayList();
                list.add(MediaType.APPLICATION_JSON);
                headers.setAccept(list);
                List<MediaType> accept = new ArrayList();
                accept.add(MediaType.ALL);
                headers.setAccept(accept);
                HttpEntity entity = new HttpEntity(payment,headers);
//                HttpEntity entity = new HttpEntity(null,headers);
                response = restTemplate.exchange(url, HttpMethod.POST,entity,Result.class);
//                String response = restTemplate.getForObject(url,String.class);
                Result result = response.getBody();
                com.bezel.android.upi.domain.Status status = com.bezel.android.upi.domain.Status.valueOf(result.getStatus().toUpperCase());
                double amount = Double.valueOf(payment.getAmount()).doubleValue();
                Transaction transaction =  new Transaction(result.getTxnId(), status,new Date(),amount);
                Log.i(TAG,"Transaction "+transaction);
                boolean value = SharedPreferenceHelper.putTransaction(transaction,getBaseContext());
                Log.i(TAG,"Data inserted into Shared Preference "+ value);
            } catch (Exception e) {
                Log.e("TransactionActivity", e.getMessage(), e);
            }

            return response;
        }

        @Override
        protected void onPostExecute(ResponseEntity<Result> responseEntity) {
            progDailog.dismiss();
            if (responseEntity!=null) {
                String status = "Payment Submitted Successfully. Your Transaction Id is %s";
                String txnId = null;
                if (responseEntity != null && responseEntity.getBody()!=null){
                    Result result = (Result)responseEntity.getBody();
                    txnId = result.getTxnId();
                }
                String formatedString = String.format(status,txnId);
                //clear the cart after successful transaction
                ShoppingCartHelper.clearCart();
                statusInfo.setText(formatedString);
            }else{
                statusInfo.setText("Error Performing Transaction.");
            }
        }

    }
}
