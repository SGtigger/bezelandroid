package com.bezel.android.upi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bezel.android.upi.R;

@SuppressLint("NewApi")
public class ViewCartFragment extends Fragment {

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater .inflate(R.layout.view_cart_fragment, container, false);
        return rootView;
    }
}