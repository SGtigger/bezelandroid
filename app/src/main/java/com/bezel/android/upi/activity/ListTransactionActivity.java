package com.bezel.android.upi.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import com.bezel.android.upi.R;
import com.bezel.android.upi.adapter.TransactionAdapter;
import com.bezel.android.upi.domain.Transaction;
import com.bezel.android.upi.helper.ListTransactionHelper;
import com.bezel.android.upi.helper.NetworkConnectivityHelper;
import com.bezel.android.upi.helper.SharedPreferenceHelper;

import java.util.List;

public class ListTransactionActivity extends Activity {

    protected static String TAG = ListTransactionActivity.class.getSimpleName();
    protected List<Transaction> transactions;
    protected Button gohometransaction;
    protected Intent homeIntent, queryIntent;
    protected ListView transactionListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_list_transaction);

//            LayoutInflater inflater = (LayoutInflater) this
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View contentView = inflater.inflate(R.layout.activity_list_transaction, null, false);
//            mDrawerLayout.addView(contentView, 0);
//            getLayoutInflater().inflate(R.layout.activity_list_transaction,frameLayout);
//            mDrawerList.setItemChecked(position,true);
            new displayTransactions().execute();

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }


    private class displayTransactions extends AsyncTask<Void, Void, List<Transaction>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            transactionListView = (ListView) findViewById(R.id.listransactionView);
            gohometransaction = (Button) findViewById(R.id.goHomeListTransactionButton);

            transactionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    new checkNetwork().execute(position);
//                    boolean isconnected = NetworkConnectivityHelper.checkInternetConnection(getApplicationContext());
//                    if (isconnected) {
                        queryIntent = new Intent(getBaseContext(), QueryTransactionActivity.class);
                        queryIntent.putExtra(ListTransactionHelper.PRODUCT_INDEX, position);
                        startActivity(queryIntent);
//                    }else {
//                        showAlertDialog(getApplicationContext(),"No Internet Connection","Check you Internet Connection!");
//                    }
                }
            });
            gohometransaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homeIntent = new Intent(getBaseContext(), HomeActivity.class);
                    startActivity(homeIntent);
                }
            });
        }

        @Override
        protected List<Transaction> doInBackground(Void... params) {
            return transactions = SharedPreferenceHelper.getTransactionList(getBaseContext());
        }

        @Override
        protected void onPostExecute(List<Transaction> transactions) {
            super.onPostExecute(transactions);
            transactionListView.setAdapter(new TransactionAdapter(transactions, getLayoutInflater()));
        }
    }

    /**
     * Function to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     */
    public void showAlertDialog(Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon(R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private class checkNetwork extends AsyncTask<Integer,Void,Boolean>{

        private int position;
        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Boolean doInBackground(Integer... params) {
            position = params[0].intValue();
            return  NetworkConnectivityHelper.checkInternetConnection(getApplicationContext());
        }


        /**
         * <p>Runs on the UI thread after {@link #doInBackground}. The
         * specified result is the value returned by {@link #doInBackground}.</p>
         * <p>
         * <p>This method won't be invoked if the task was cancelled.</p>
         *
         * @param isconnected The result of the operation computed by {@link #doInBackground}.
         * @see #onPreExecute
         * @see #doInBackground
         * @see #onCancelled(Object)
         */
        @Override
        protected void onPostExecute(Boolean isconnected) {
            super.onPostExecute(isconnected);
            if (isconnected) {
                queryIntent = new Intent(getBaseContext(), QueryTransactionActivity.class);
                queryIntent.putExtra(ListTransactionHelper.PRODUCT_INDEX, position);
                startActivity(queryIntent);
            }else {
                showAlertDialog(getApplicationContext(),"No Internet Connection","Check you Internet Connection!");
            }

        }
    }

}
