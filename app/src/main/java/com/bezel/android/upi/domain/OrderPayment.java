package com.bezel.android.upi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class OrderPayment implements Serializable{

    @JsonProperty("refId")
    private String refId;

    @JsonProperty(value="refUrl", required=false)
    private String refUrl;

    @JsonProperty(value="note", required=true)
    private String note;

    @JsonProperty(value="payer",  required=true)
    private String payerVirtualAddr;

    @JsonProperty(value="payees", required=true)
    private Payees payees;

    @JsonProperty(value="amount", required=true)
    private String amount;

    @JsonProperty(value="currency", required=true)
    private String currency;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefUrl() {
        return refUrl;
    }

    public void setRefUrl(String refUrl) {
        this.refUrl = refUrl;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPayerVirtualAddr() {
        return payerVirtualAddr;
    }

    public void setPayerVirtualAddr(String payerVirtualAddr) {
        this.payerVirtualAddr = payerVirtualAddr;
    }

    public Payees getPayees() {
        return payees;
    }

    public void setPayees(Payees payees) {
        this.payees = payees;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}