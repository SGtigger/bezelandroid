package com.bezel.android.upi.adapter;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.bezel.android.upi.R;
import com.bezel.android.upi.domain.Product;

import java.util.List;

/**
 * Created by ahamed.nijamudeen on 3/3/2016.
 */
public class ProductAdapter extends BaseAdapter{

    private List<Product> mProductList;
    private LayoutInflater mInflater;
    private boolean mShowCheckbox;
    private AssetManager asset;

    public ProductAdapter(List<Product> list, LayoutInflater inflater, boolean showCheckbox, AssetManager asset) {
        mProductList = list;
        mInflater = inflater;
        mShowCheckbox = showCheckbox;
        this.asset = asset;
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    @Override
    public Object getItem(int position) {
        return mProductList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewItem item;
        Typeface typefaceRupee = Typeface.createFromAsset(asset,"fonts/IndianRupee.ttf");

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.activity_item,
                    null);
            item = new ViewItem();

            item.productImageView = (ImageView) convertView
                    .findViewById(R.id.ImageView);

            item.productTitle = (TextView) convertView.findViewById(R.id.titleTextView);

            item.productCheckbox = (CheckBox) convertView.findViewById(R.id.CheckBoxSelected);

            item.price = (TextView) convertView.findViewById(R.id.priceTextView);

            item.quantity = (TextView) convertView.findViewById(R.id.quantityTextView);

            convertView.setTag(item);
        } else {
            item = (ViewItem) convertView.getTag();
        }

        Product curProduct = mProductList.get(position);

        item.productImageView.setImageDrawable(curProduct.productImage);
        item.productTitle.setText(curProduct.title);
        item.quantity.setText(curProduct.quantity);
        item.price.setTypeface(typefaceRupee);
        item.price.setText("Rs. "+String.valueOf(curProduct.price));

        if(!mShowCheckbox) {
            item.productCheckbox.setVisibility(View.GONE);
        } else {
            if(curProduct.selected == true)
                item.productCheckbox.setChecked(true);
            else
                item.productCheckbox.setChecked(false);
        }


        return convertView;
    }


    private class ViewItem {
        ImageView productImageView;
        TextView productTitle;
        TextView price;
        TextView quantity;
        CheckBox productCheckbox;
    }
}
