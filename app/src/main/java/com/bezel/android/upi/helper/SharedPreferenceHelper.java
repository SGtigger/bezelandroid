package com.bezel.android.upi.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.bezel.android.upi.domain.Transaction;
import com.bezel.android.upi.json.JsonHelper;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ahamed.nijamudeen on 19/3/2016.
 */
public class SharedPreferenceHelper {
    private static final String TAG = SharedPreferenceHelper.class.getSimpleName();

    private static final String fileKey = "BezelPay";
    private static final String key = "transaction";
    private static List<Transaction> transactions = new ArrayList<Transaction>();

    public static SharedPreferences getSharedPreference(Context context){
        return context.getSharedPreferences(fileKey,Context.MODE_PRIVATE);
    }

    public static boolean putTransaction(Transaction value, Context context){
        List<Transaction> transanctionList = getTransactionList(context);
        transanctionList.add(value);
        Collections.sort(transanctionList);
        //store only top 5 transactions
        String data = JsonHelper.objToJson(transanctionList.subList(0,5));
        SharedPreferences.Editor editor = getSharedPreference(context).edit().putString(key,data);
        return editor.commit();
    }

    public static List<Transaction> getTransactionList(Context context){
        List<Transaction> list = null;
        String data = getSharedPreference(context).getString(key, null);
        if (data ==null){
            list = transactions;
        }else{
            //Convert the String into object Using Gson
            Type listType = new TypeToken<ArrayList<Transaction>>() {}.getType();
            list = JsonHelper.getGson().fromJson(data,listType);
            Collections.sort(list);
            Log.i(TAG,"List ["+list+"]");
        }
        return list;
    }
}
