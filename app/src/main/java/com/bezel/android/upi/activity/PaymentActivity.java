package com.bezel.android.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.bezel.android.upi.R;
import com.bezel.android.upi.domain.OrderPayment;
import com.bezel.android.upi.domain.Payee;
import com.bezel.android.upi.domain.Payees;

import java.text.DecimalFormat;

/**
 * Created by ahamed.nijamudeen on 3/7/2016.
 */
public class PaymentActivity extends Activity {
    private Button payUsingBezelPay,payUsingCreditCard,payBezel,cancelPay;
    private TextView productPrice,taxValue,totalPrice,currency,bezelServiceCharge,totalAmount,payeeValue;
    private EditText payerValue;
    private TableLayout bezelPayTable;
    private double productamount,tax,totalPayable,amountIncludingServiceChrg;
    private double bezelServiceAmount = 2.5; //Dollar
    private DecimalFormat df = new DecimalFormat("0.00");

    private static final String TAG = PaymentActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.payment_activity);
//            LayoutInflater inflater = (LayoutInflater) this
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View contentView = inflater.inflate(R.layout.payment_activity, null, false);

//            mDrawerLayout.addView(contentView, 0);
//            getLayoutInflater().inflate(R.layout.payment_activity,frameLayout);
//            mDrawerList.setItemChecked(position,true);

            final Typeface typefaceRupee = Typeface.createFromAsset(getAssets(),"fonts/IndianRupee.ttf");
            payUsingBezelPay = (Button) findViewById(R.id.bezelPayButton);
            payUsingCreditCard = (Button) findViewById(R.id.creditCardButton);
            payBezel = (Button) findViewById(R.id.payBezelButton);
            productPrice = (TextView) findViewById(R.id.productValueTextView);
            taxValue = (TextView) findViewById(R.id.taxValueTextView);
            totalPrice = (TextView) findViewById(R.id.totalValueTextView);
            payerValue = (EditText) findViewById(R.id.payerValueTextView);
            payeeValue = (TextView) findViewById(R.id.payeeValueTextView);
            currency = (TextView) findViewById(R.id.currencyValueTextView);
            bezelServiceCharge = (TextView) findViewById(R.id.chargeValueTextView);
            totalAmount = (TextView) findViewById(R.id.amoutValueTextView);
            bezelPayTable = (TableLayout) findViewById(R.id.bezelpayTableView);
            //Disable the Table on loading
            bezelPayTable.setVisibility(View.INVISIBLE);

            productamount = getIntent().getExtras().getDouble("TOTAL_PRODUCT_PRICE");
            tax = getIntent().getExtras().getDouble("GROSS_SERVICE_TAX");
            totalPayable = getIntent().getExtras().getDouble("TOTAL_PAYABLE_AMOUNT");
            bezelServiceCharge.setText(String.valueOf(bezelServiceAmount));
            productPrice.setTypeface(typefaceRupee);
            productPrice.setText("Rs. "+df.format(productamount));
            taxValue.setTypeface(typefaceRupee);
            taxValue.setText("Rs. "+df.format(tax));
            totalPrice.setTypeface(typefaceRupee);
            totalPrice.setText("Rs. "+df.format(totalPayable));
            amountIncludingServiceChrg = totalPayable + bezelServiceAmount;

            payUsingBezelPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (bezelPayTable.getVisibility()==View.VISIBLE){
                        bezelPayTable.setVisibility(View.INVISIBLE);
                    }else {
                        currency.setText(R.string.indian_currency_INR);
                        bezelServiceCharge.setTypeface(typefaceRupee);
                        bezelServiceCharge.setText("Rs. "+df.format(bezelServiceAmount));
                        totalAmount.setTypeface(typefaceRupee);
                        totalAmount.setText("Rs. "+df.format(amountIncludingServiceChrg));
                        bezelPayTable.setVisibility(View.VISIBLE);
                    }
                }
            });


            payUsingCreditCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bezelPayTable.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(),"Pay Using Credit Card NOT Implemented!",Toast.LENGTH_LONG).show();
                }
            });

            payBezel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent bezelPayIntent =  new Intent(PaymentActivity.this,TransactionActivity.class);
                    OrderPayment payment = new OrderPayment();
                    payment.setAmount(df.format(amountIncludingServiceChrg));
                    payment.setCurrency("INR");
                    payment.setPayerVirtualAddr(payerValue.getText().toString());
                    payment.setRefId("consumerRef 1");
                    payment.setRefUrl("http://localhost/xxx");
                    payment.setNote("pay to Bezel payer1 for item a");
                    Payee payee = new Payee();
                    payee.setVirtualAddr(payeeValue.getText().toString());
                    Payees payees = new Payees();
                    payees.add(payee);
                    payment.setPayees(payees);
                    Log.i(PaymentActivity.class.getSimpleName(),"Pay Bezel Button is Clicked");
                    Intent bezelPayIntent =  new Intent(PaymentActivity.this,TransactionActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("PaymentOrder",payment);
                    bezelPayIntent.putExtras(bundle);
                    startActivity(bezelPayIntent);
                }
            });

            cancelPay = (Button) findViewById(R.id.cancelPayButton);
            cancelPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent cancelPayIntent = new Intent(getBaseContext(),ShoppingCartActivity.class);
                    startActivity(cancelPayIntent);
                }
            });
        } catch (Exception e) {
            Log.e(TAG,e.getMessage(),e);
        }
    }
}
