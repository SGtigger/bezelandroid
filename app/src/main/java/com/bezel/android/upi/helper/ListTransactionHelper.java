package com.bezel.android.upi.helper;

import android.content.Context;
import com.bezel.android.upi.domain.Transaction;

import java.util.List;

/**
 * Created by ahamed.nijamudeen on 3/15/2016.
 */
public class ListTransactionHelper {

    public static final String PRODUCT_INDEX = "PRODUCT_INDEX";
    private static List<Transaction> transactions;

    public static List<Transaction> getTransactions(Context context){
//        if (transactions == null){
//            transactions = new ArrayList<>();
//            transactions.add(new Transaction("WKYPZKPNW3H46UQJIGDIFCAG61WZ7BGQ", Status.FAILED,new Date(),55.60));
//            transactions.add(new Transaction(UUID.randomUUID().toString(), Status.SUCCESS,new Date(),65.45));
//            transactions.add(new Transaction(UUID.randomUUID().toString(), Status.PENDING,new Date(),48.52));
//            transactions.add(new Transaction(UUID.randomUUID().toString(), Status.PROCESSING,new Date(),58.90));
//        }
//        return transactions;
        return SharedPreferenceHelper.getTransactionList(context);
    }

    public static Transaction get(int position, Context context){
        return getTransactions(context).get(position);
    }
}
