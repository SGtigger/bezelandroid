package com.bezel.android.upi.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bezel.android.upi.R;
import com.bezel.android.upi.domain.Product;
import com.bezel.android.upi.helper.ShoppingCartHelper;

import java.util.List;

/**
 * Created by ahamed.nijamudeen on 3/3/2016.
 */
public class ProductDetailsActivity extends Activity {

    private static final String TAG = ProductDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.productdetails);
//            LayoutInflater inflater = (LayoutInflater) this
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View contentView = inflater.inflate(R.layout.productdetails, null, false);

//            getLayoutInflater().inflate(R.layout.productdetails,frameLayout);
//            mDrawerList.setItemChecked(position,true);

            List<Product> catalog = ShoppingCartHelper.getCatalog(getResources());
            final List<Product> cart = ShoppingCartHelper.getCart();

            int productIndex = getIntent().getExtras().getInt(ShoppingCartHelper.PRODUCT_INDEX);
            final Product selectedProduct = catalog.get(productIndex);

            // Set the proper image and text
            ImageView productImageView = (ImageView) findViewById(R.id.ImageProduct);
            productImageView.setImageDrawable(selectedProduct.productImage);
            TextView productTitleTextView = (TextView) findViewById(R.id.TextProductTitle);
            productTitleTextView.setText(selectedProduct.title);
            TextView productDetailsTextView = (TextView) findViewById(R.id.TextProductDetails);
            productDetailsTextView.setText(selectedProduct.description);
            TextView productQuantity = (TextView) findViewById(R.id.TextProductQuantity);
            productQuantity.setText(selectedProduct.quantity);
            TextView productPrice = (TextView) findViewById(R.id.TextProductPrice);
            productPrice.setText("Rs. "+String.valueOf(selectedProduct.price));

            Button addToCartButton = (Button) findViewById(R.id.ButtonAddToCart);
            addToCartButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {


                    cart.add(selectedProduct);
                    finish();
                }
            });

            Button cancelProductDetail = (Button) findViewById(R.id.CancelProductDetail);
            cancelProductDetail.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            // Disable the add to cart button if the item is already in the cart
            if (cart.contains(selectedProduct)) {
                addToCartButton.setEnabled(false);
                addToCartButton.setText("Item in Cart");
            }
        } catch (Exception e) {
            Log.e(TAG,e.getMessage(),e);
        }
    }

}
