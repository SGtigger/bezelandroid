package com.bezel.android.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.bezel.android.upi.R;

public class HomeActivity extends Activity {

    Button product,viewCart,transactions,logout;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        product = (Button) findViewById(R.id.HomeProductCatalogButton);
        viewCart = (Button) findViewById(R.id.HomeViewCartButton);
        transactions = (Button) findViewById(R.id.HomeTransactionsButton);
        logout = (Button) findViewById(R.id.HomeLogoutButton);

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getBaseContext(),CatalogActivity.class);
                startActivity(intent);
            }
        });

        viewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getBaseContext(),ShoppingCartActivity.class);
                startActivity(intent);
            }
        });

        transactions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getBaseContext(),ListTransactionActivity.class);
                startActivity(intent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getBaseContext(),LogoutActivity.class);
                startActivity(intent);
            }
        });
    }

}
