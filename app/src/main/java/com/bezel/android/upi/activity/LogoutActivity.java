package com.bezel.android.upi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.bezel.android.upi.R;

public class LogoutActivity extends Activity {

    private static final String TAG = LogoutActivity.class.getSimpleName();

    Button login;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"Logging Out...");
        setContentView(R.layout.activity_logout);
        login = (Button) findViewById(R.id.GoLoginButton);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getBaseContext(),LoginActivity.class);
                startActivity(intent);
            }
        });
    }

}
