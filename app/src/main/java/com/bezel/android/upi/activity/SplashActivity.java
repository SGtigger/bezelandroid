package com.bezel.android.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import com.bezel.android.upi.R;

/**
 * Created by Ahamed.Nijamudeen on 3/2/2016.
 */
public class SplashActivity extends Activity{

    private static String TAG =  SplashActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.splash);
        super.onCreate(savedInstanceState);
        new Wait().execute();
//        try {
//            setContentView(R.layout.splash);
//            Thread timerThread = new Thread(){
//                public void run(){
//                    try{
//                        sleep(3000);
//                    }catch(InterruptedException e){
//                        e.printStackTrace();
//                    }finally{
//                        Intent homeIntent = new Intent(SplashActivity.this,LoginActivity.class);
//                        startActivity(homeIntent);
//                    }
//                }
//            };
//            timerThread.start();
//        } catch (Exception e) {
//            Log.e(TAG,"Error Processing Splash Screen",e);
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private class Wait extends AsyncTask<Void,Void,Void>{
        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(TAG,e.getMessage(),e);
            }
            return null;
        }

        /**
         * <p>Runs on the UI thread after {@link #doInBackground}. The
         * specified result is the value returned by {@link #doInBackground}.</p>
         * <p>
         * <p>This method won't be invoked if the task was cancelled.</p>
         *
         * @param aVoid The result of the operation computed by {@link #doInBackground}.
         * @see #onPreExecute
         * @see #doInBackground
         * @see #onCancelled(Object)
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
                Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
        }
    }
}
