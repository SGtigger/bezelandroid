package com.bezel.android.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.bezel.android.upi.R;

public class LoginActivity extends Activity {

    Button b1, b2;
    EditText ed1, ed2;

    TextView tx1;
    int counter = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_login);

            b1 = (Button) findViewById(R.id.loginbutton);
            ed1 = (EditText) findViewById(R.id.usernameEditText);
            ed2 = (EditText) findViewById(R.id.passwordEditText);

            b2 = (Button) findViewById(R.id.loginresetButton);
            tx1 = (TextView) findViewById(R.id.loginMessageTextView);
            tx1.setVisibility(View.GONE);

            b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tx1.setText("");
                    if (ed1.getText().toString().equals("demo") &&

                            ed2.getText().toString().equals("demo")) {
                        Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
//                        Intent homeIntent = new Intent(LoginActivity.this, BaseActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_SHORT).show();
                        tx1.setVisibility(View.VISIBLE);
                        tx1.setText("Invalid Username/Password!");
                        tx1.setTextColor(Color.RED);
                        tx1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    }
                }
            });


            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ed1.setText("");
                    ed2.setText("");
                    tx1.setText("");
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}