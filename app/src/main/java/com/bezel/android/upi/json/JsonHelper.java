package com.bezel.android.upi.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by ahamed.nijamudeen on 19/3/2016.
 */
public class JsonHelper {

    private static Gson gson = new GsonBuilder().create();

    public static String objToJson(Object obj){
        return gson.toJson(obj);
    }

    public static <T> T jsonToObj(String data ,Class<T> clazz){
        return gson.fromJson(data,clazz);
    }

    public static Gson getGson(){
        return gson;
    }
}
