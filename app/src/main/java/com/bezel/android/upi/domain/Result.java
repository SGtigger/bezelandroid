package com.bezel.android.upi.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Ahamed.Nijamudeen on 3/14/2016.
 */
public class Result {
    @JsonProperty("message")
    private String msg;

    @JsonProperty("status")
    private String status;

    @JsonProperty("tnxId")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String txnId;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }
}