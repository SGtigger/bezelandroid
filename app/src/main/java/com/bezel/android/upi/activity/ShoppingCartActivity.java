package com.bezel.android.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.bezel.android.upi.R;
import com.bezel.android.upi.adapter.ProductAdapter;
import com.bezel.android.upi.domain.Product;
import com.bezel.android.upi.helper.ShoppingCartHelper;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by ahamed.nijamudeen on 3/3/2016.
 */
public class ShoppingCartActivity extends Activity {

    private static String TAG = ShoppingCartActivity.class.getSimpleName();
    private List<Product> mCartList;
    private ProductAdapter mProductAdapter;
    private double productPrice,tax,totalPayable;
    private TextView totalProductPrice,totaltax,totalPayableView;
    private Typeface typefaceRupee;
    Button goProduct,checkoutPay,removeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_shoppingcart);
//            LayoutInflater inflater = (LayoutInflater) this
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View contentView = inflater.inflate(R.layout.activity_shoppingcart, null, false);

//            getLayoutInflater().inflate(R.layout.activity_shoppingcart,frameLayout);
//            mDrawerList.setItemChecked(position,true);

            typefaceRupee = Typeface.createFromAsset(getAssets(),"fonts/IndianRupee.ttf");

            // Create the list
            final ListView listViewCatalog = (ListView) findViewById(R.id.ShoppingCartListView);
            mCartList = ShoppingCartHelper.getCart();
            mProductAdapter = new ProductAdapter(mCartList, getLayoutInflater(), true,getAssets());
            listViewCatalog.setAdapter(mProductAdapter);
            totalProductPrice = (TextView) findViewById(R.id.productValueTextView);
            totaltax = (TextView) findViewById(R.id.taxValueTextView);
            totalPayableView = (TextView) findViewById(R.id.totalValueTextView);

            productPrice = 0.0;
            tax = 0.0;
            totalPayable = 0.0;

            // Make sure to clear the selections
            for (int i = 0; i < mCartList.size(); i++) {
                mCartList.get(i).selected = false;
                productPrice = productPrice + mCartList.get(i).getPrice();
            }
            // calculate 7 % GST for the total amount
            tax = productPrice * 0.07;
            totalPayable = productPrice + tax;

            DecimalFormat df = new DecimalFormat("0.00");
            totalProductPrice.setTypeface(typefaceRupee);
            totalProductPrice.setText("Rs. "+df.format(productPrice));
            totaltax.setTypeface(typefaceRupee);
            totaltax.setText("Rs. "+df.format(tax));
            totalPayableView.setTypeface(typefaceRupee);
            totalPayableView.setText("Rs. "+df.format(totalPayable));

//            calculateCartValue();
//            setUIValue();

            listViewCatalog.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {

                    Product selectedProduct = mCartList.get(position);
                    if (selectedProduct.selected)
                        selectedProduct.selected = false;
                    else
                        selectedProduct.selected = true;

                    mProductAdapter.notifyDataSetInvalidated();

                }
            });

            checkoutPay = (Button) findViewById(R.id.checkoutpayButton);

            removeButton = (Button) findViewById(R.id.ButtonRemoveFromCart);
            removeButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Loop through and remove all the products that are selected
                    // Loop backwards so that the remove works correctly
                    for (int i = mCartList.size() - 1; i >= 0; i--) {

                        if (mCartList.get(i).selected) {
                            mCartList.remove(i);
                        }
                    }
                    calculateCartValue();
                    setUIValue();
                    if (mCartList.size()==0){
                        removeButton.setEnabled(false);
                        checkoutPay.setEnabled(false);
                    }else{
                        removeButton.setEnabled(true);
                        checkoutPay.setEnabled(true);
                    }
                    mProductAdapter.notifyDataSetChanged();
                }
            });


            checkoutPay.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ShoppingCartActivity.this, PaymentActivity.class);
                    intent.putExtra("TOTAL_PRODUCT_PRICE", productPrice);
                    intent.putExtra("GROSS_SERVICE_TAX", tax);
                    intent.putExtra("TOTAL_PAYABLE_AMOUNT", totalPayable);
                    startActivity(intent);

                }
            });
            goProduct = (Button) findViewById(R.id.goProductButton);
            goProduct.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goProductIntent = new Intent(getBaseContext(),CatalogActivity.class);
                    startActivity(goProductIntent);
                }
            });
            //Disable the Button if no item is present in the Cart
            if (mCartList.size()==0){
                removeButton.setEnabled(false);
                checkoutPay.setEnabled(false);
            }else{
                removeButton.setEnabled(true);
                checkoutPay.setEnabled(true);
            }

        } catch (Exception e) {
            Log.e(TAG,e.getMessage(),e);
        }

    }

    private void setUIValue(){
        DecimalFormat df = new DecimalFormat("0.00");
        totalProductPrice.setTypeface(typefaceRupee);
        totalProductPrice.setText("Rs. "+df.format(productPrice));
        totaltax.setTypeface(typefaceRupee);
        totaltax.setText("Rs. "+df.format(tax));
        totalPayableView.setTypeface(typefaceRupee);
        totalPayableView.setText("Rs. "+df.format(totalPayable));
    }

    private void calculateCartValue(){

        productPrice = 0.0;
        tax = 0.0;
        totalPayable = 0.0;

        // Make sure to clear the selections
        for (int i = 0; i < mCartList.size(); i++) {
            mCartList.get(i).selected = false;
            productPrice = productPrice + mCartList.get(i).getPrice();
        }
        // calculate 7 % GST for the total amount
        tax = productPrice * 0.07;
        totalPayable = productPrice + tax;

    }

}
